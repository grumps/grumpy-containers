#!/usr/bin/env make
override CONTAINERS = $(patsubst src/%,%,$(wildcard src/*))
override TARGETS = $(patsubst src/%,%,$(subst targets/,,$(wildcard src/**/targets/*)))

override PUSH_TARGETS = $(addprefix push/,$(TARGETS))
override BUILD_TARGETS = $(addprefix build/,$(TARGETS))

override BUILD_CONTAINERS = $(addprefix build/,$(CONTAINERS))
override PUSH_CONTAINERS = $(addprefix push/,$(CONTAINERS))

override REGISTRY_URL = registry.gitlab.com/grumps/grumpy-containers

TAG = $(shell cat src/$(@F)/VERSION)
BASE_NAME = $(notdir $(@D))

all: $(BUILD_CONTAINERS) $(BUILD_TARGETS) $(PUSH_CONTAINERS) $(PUSH_TARGETS)

$(BUILD_CONTAINERS): TAG = $(shell cat src/$(@F)/VERSION)
$(BUILD_CONTAINERS):
	docker build -t $(REGISTRY_URL)/$(@F):$(TAG) -f src/$(@F)/Dockerfile src/$(@F)
	@echo "$(@F)/$(TAG) has been built"

$(PUSH_CONTAINERS): TAG = $(shell cat src/$(@F)/VERSION)
$(PUSH_CONTAINERS):
	docker push $(REGISTRY_URL)/$(@F):$(TAG)
	@echo "$(@F)/$(TAG) has been pushed"

$(BUILD_TARGETS): TAG = $(shell cat src/$(BASE_NAME)/VERSION)
$(BUILD_TARGETS):
	docker build --target $(@F) -t $(REGISTRY_URL)/$(BASE_NAME):$(TAG)-$(@F) -f src/$(BASE_NAME)/Dockerfile src/$(BASE_NAME)
	@echo "$(BASE_NAME)/$(@F)/$(TAG) has been built"

$(PUSH_TARGETS): TAG = $(shell cat src/$(BASE_NAME)/VERSION)
$(PUSH_TARGETS):
	docker push $(REGISTRY_URL)/$(BASE_NAME):$(TAG)-$(@F)
	@echo "$(BASE_NAME)/$(@F)/$(TAG) has been pushed"


build: $(BUILD_CONTAINERS) $(BUILD_TARGETS)
	@echo "completed builds"

push: $(PUSH_CONTAINERS) $(PUSH_TARGETS)
	@echo "pushed all containers"

%: build/% push/%
	echo "completed $%"
