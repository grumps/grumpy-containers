#!/bin/bash

cmd="${1}"
case "${cmd}" in
    "pause")
        exec pause;;
    "echoserver")
        exec go-echo;;
    *) echo >&2 "Invalid option: $@"; exit 1;;
esac
