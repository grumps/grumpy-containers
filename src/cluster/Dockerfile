ARG BASE_VER=latest
FROM gcr.io/google-containers/pause:latest

FROM golang:1.17-bullseye                                          
WORKDIR /app                                                       
RUN git clone https://git.ofmax.li/go-echo \                       
        && cd go-echo \                                            
        && go build -o /go-echo                                    
                                                                   
FROM registry.gitlab.com/grumps/grumpy-containers/base:${BASE_VER} 
ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update -y \
        && apt-get upgrade -y \
                && apt-get install -y curl dnsutils jq vim kubernetes-client tmux ldap-utils ncat nmap hping3 python3-minimal unattended-upgrades \
                && unattended-upgrades
COPY --from=0 /pause /usr/local/bin/pause
COPY --from=1 /app/go-echo /usr/local/bin/go-echo
COPY entrypoint.sh /usr/local/bin/entrypoint.sh

USER app
WORKDIR /opt/app
ENTRYPOINT ["/usr/local/bin/entrypoint.sh"]
CMD ["pause"]
