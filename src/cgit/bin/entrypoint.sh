#!/usr/bin/env bash

cat << EOF > /etc/nginx/sites-enabled/default
server {
    listen $CGIT_PORT;
    server_name $CGIT_HOSTNAME;
    server_tokens off;
    root /usr/share/cgit;

    # Serve static files with nginx
    location ~* ^.+(cgit.(css|png)|favicon.ico|robots.txt) {
      root /usr/share/cgit;
      expires 30d;
    }
    location / {
            try_files \$uri @cgit;
    }
    location @cgit {
      gzip off;
      include uwsgi_params;
      uwsgi_modifier1 9;
      uwsgi_pass unix:/run/uwsgi/cgit.sock;
    }
}
EOF

cat << EOF > /etc/cgitrc
#
# cgit config
# see cgitrc(5) for details

root-title=git.ofmax.li
root-desc=grumpy software

readme=:README.md

about-filter=/usr/lib/cgit/filters/about-formatting.sh
source-filter=/usr/lib/cgit/filters/syntax-highlighting.py

enable-index-links=1
enable-log-filecount=1
enable-commit-graph=1
enable-index-owner=1
enable-http-clone=1
snapshots=tar.gz
css=/cgit.css
logo=/cgit.png
virtual-root=/
robots=nofollow
remove-suffix=1

project-list=/var/lib/git/projects.list
scan-path=/var/lib/git/repositories

EOF

uwsgi --daemonize --ini /etc/uwsgi/cgit.ini > /dev/stdout 2> /dev/stderr < /dev/null &
nginx -c /etc/nginx/nginx.conf -g "daemon off;"
